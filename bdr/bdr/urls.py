"""bdr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from bdr_app.views import *
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', inicio),
    path('realizar-consultas/', realizar_consultas),
    path('listar-cliente', listar_cliente),
    path('listar-banco', listar_banco),
    path('carga', carga),
    path('apagar', apagar),
    path('exibir-cliente', exibir_clientes),
    path('editar-cliente', editar_cliente),
    path('listar-periodo', periodo),
    path('exibir-banco', exibir_bancos),
    path('editar-banco', editar_banco),
    path('cadastrar-banco', cadastrar_banco),
    path('apagar-extrato', apagar_extrato),
    path('gerar-relatorio', gerar_relatorio),
    path('inserir-extrato', inserir_extrato),
    path('relatorio2', relatorio2),
    path('relatorio1', relatorio1)

]
