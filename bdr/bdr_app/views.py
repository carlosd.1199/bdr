from django.shortcuts import render, redirect
import mysql.connector
import subprocess
import json
import os
from time import time
# Create your views here.
def inicio(request):
	return render(request, 'inicio.html', {})

def realizar_consultas(request):
	return render(request, 'dashboard_consultas.html', {})


def handle_uploaded_file(file, filename, banco):
	if not os.path.exists('CONTAS/'+ banco + '/EXTRATOS'):
		os.mkdir('CONTAS/'+ banco + '/EXTRATOS')
	with open('CONTAS/'+ banco + '/EXTRATOS/' + str(time()) + filename, 'wb+') as destination:
		for chunk in file.chunks():
			destination.write(chunk)


def inserir_extrato(request):
	mensagem = None
	try:
		#print(request.FILES.getlist('arquivo[]'))
		if request.POST['banco']:
			try:
				for file in request.FILES.getlist('arquivo[]'):
					handle_uploaded_file(file, str(file), request.POST['banco'])
				mensagem = "Arquivos inseridos com sucesso. Para atualizar o banco de dados, vá até a página inicial e clique em CARGA DE DADOS."
			except:
				mensagem = 'Ocorreu um erro na sua solicitação. Por favor, envie novamente os arquivos.'
	except Exception as e:
		print(e)
	return render(request, "inserir_extrato.html", {'mensagem':mensagem})


def exibir_bancos(request):
	try:
		resposta = request.POST['r']
	except:
		resposta = None
	
	
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	bancos = []
	lista = []
	mycursor = mydb.cursor()
	mycursor.execute("SELECT * FROM BANCO ORDER BY NR;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['id'] = dado[0]
		temp['descricao'] = dado[1]
		bancos.append(temp)
	return render(request, 'exibir_bancos.html', {'bancos': bancos})
def cadastrar_banco(request):
	try:
		id = request.POST['id']
		descricao = request.POST['descricao']
	except:
		id = ''
		descricao = ''
	mensagem = None
	if id != '' and descricao != '':
		mydb = mysql.connector.connect(
		host="localhost",
		user="root",
		database="bdr",
		password="1234",
		auth_plugin='mysql_native_password'
		)
		mycursor = mydb.cursor()
		try:
			mycursor.execute("INSERT INTO BANCO(NR, DESCRICAO) VALUES(%s, %s);", (id, descricao))
			mydb.commit()
			mensagem = 'Banco cadastrado com sucesso!'
		except Exception as e:
			mensagem = "Não foi possível inserir este banco. Chave já cadastrada!"
			print(e)
	return render(request, "cadastrar_banco.html", {"mensagem": mensagem, 'id': id, 'descricao': descricao})
			

def editar_banco(request):
	descricao = ''
	try:
		request.GET['l']
		local = 'get'
	except:
		try:
			request.POST['l']
			local = 'post'
		except:
			redirect('exibir-bancos/')
	if local == 'get':	
		try:
			chave = request.GET['id']
		except:
			chave = ''
	elif local == 'post':
		try:
			chave = request.POST['numero']
			descricao = request.POST['descricao']
		except:
			chave = ''

	try:
		verificar = request.POST['verificar']
	except:
		verificar = False

	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	mycursor.execute("SELECT * FROM BANCO WHERE NR LIKE %s;", (chave,))
	dados = mycursor.fetchall()
	
	mensagem = None
	if verificar != False:
		try:
			mycursor.execute("UPDATE BANCO SET DESCRICAO = %s WHERE NR LIKE %s;", (descricao, chave))
			mydb.commit()
			mensagem = "Campo alterado com sucesso!"
		except:
			mensagem = "Ocorreu um erro na solicitação."
	elif dados != []:
		descricao = dados[0][1]
	return render(request, 'editar_banco.html', {"numero": chave, "descricao": descricao, "mensagem": mensagem})


def listar_banco(request):
	total = 0.00
	try:
		id_banco = request.POST['id']
	except:
		id_banco = None
	
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	bancos = []
	lista = []
	mycursor = mydb.cursor()
	mycursor.execute("SELECT * FROM BANCO ORDER BY NR;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['id'] = dado[0]
		temp['nome'] = dado[1]
		bancos.append(temp)
	if id_banco != None:
		lista = []
		if id_banco == 0 or id_banco == "0":
			mycursor.execute("SELECT * FROM LANCAMENTOS;")
		else:
			mycursor.execute("SELECT * FROM LANCAMENTOS WHERE BANCO LIKE %s;", (id_banco,))
		
		dados = mycursor.fetchall()
		for dado in dados:
			temp = {}
			temp['banco'] = dado[0]
			temp['conta'] = dado[1]
			temp['id'] = dado[2]
			temp['descricao'] = dado[3]
			temp['operacao'] = dado[4]
			temp['valor_real'] = dado[5]
			temp['valor_dolar'] = dado[6]
			temp['tag'] = dado[9]
			temp['vencimento'] = str(dado[11].day) + '/' + str(dado[11].month) + '/' + str(dado[11].year)
			if 'Pagamentos' not in temp['tag']:
				total += temp['valor_real']
			lista.append(temp)
	#lista = json.dumps(lista)
	return render(request, 'listar_banco.html', {'linhas': lista, 'bancos': bancos, 'id_banco': id_banco, 'total': round(total,2)})

def periodo(request):
	try:
		l = request.POST['id'].split("+")
		conta = l[0]
		banco = l[1]
		inicio = request.POST['data_inicio']
		final = request.POST['data_final']
		tag_selecionada = request.POST['tags']
	except KeyError:
		conta = None
		banco = None
		inicio = ""
		final = ""
		tag_selecionada = ''
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	clientes = []
	lista = []
	mycursor = mydb.cursor()
	#mycursor.execute("SELECT * FROM LANCAMENTOS where valor_real > 0 GROUP BY TAG;")
	mycursor.execute("SELECT * FROM CONTA ORDER BY CLIENTE;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['conta'] = dado[0]
		temp['nome'] = dado[1]
		temp['banco'] = str(dado[4])
		temp['descricao'] = dado[2]
		clientes.append(temp)
	total = 0.00
	tags = []
	mycursor.execute("SELECT DISTINCT TAG FROM LANCAMENTOS ORDER BY TAG ASC;")
	dados = mycursor.fetchall()
	for dado in dados:
		tags.append(dado[0])
	if conta != None and banco != None:
		if conta == "0" and banco == "0":
			if tag_selecionada == 'tags' or tag_selecionada == '':
				mycursor.execute("SELECT * FROM LANCAMENTOS WHERE DATA_VENCIMENTO >= %s and DATA_VENCIMENTO <= %s;", (inicio, final))
			else:
				mycursor.execute("SELECT * FROM LANCAMENTOS WHERE DATA_VENCIMENTO >= %s and DATA_VENCIMENTO <= %s and TAG like %s;", (inicio, final, tag_selecionada))

		else:
			if tag_selecionada == 'tags' or tag_selecionada == '':
				mycursor.execute("SELECT * FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s and DATA_VENCIMENTO >= %s and DATA_VENCIMENTO <= %s;", (conta, banco, inicio, final))
			else:
				mycursor.execute("SELECT * FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s and DATA_VENCIMENTO >= %s and DATA_VENCIMENTO <= %s and TAG like %s;", (conta, banco, inicio, final, tag_selecionada))
		dados = mycursor.fetchall()
		for dado in dados:
			temp = {}
			temp['banco'] = dado[0]
			temp['conta'] = dado[1]
			temp['id'] = dado[2]
			temp['descricao'] = dado[3]
			temp['operacao'] = dado[4]
			temp['valor_real'] = dado[5]
			temp['valor_dolar'] = dado[6]
			temp['tag'] = dado[9]
			temp['vencimento'] = str(dado[11].day) + '/' + str(dado[11].month) + '/' + str(dado[11].year)
			if 'Pagamentos' not in temp['tag']:
				total += temp['valor_real']
			lista.append(temp)
	return render(request, 'listar_periodo.html', {"clientes": clientes, 'linhas': lista, 'id_conta': str(conta), 'id_banco': str(banco), "inicio": inicio, "final": final,"tag_selecionada": tag_selecionada, "tags_full": tags, 'total': round(total,2)})
def exibir_clientes(request):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	mycursor.execute("SELECT * FROM CONTA;")
	dados = mycursor.fetchall()
	clientes = []
	for dado in dados:
		temp = {}
		temp['banco'] = dado[4]
		temp['conta'] = dado[0]
		temp['cliente'] = dado[1]
		temp['descricao'] = dado[2]
		clientes.append(temp)
	return render(request, 'exibir_clientes.html', {'clientes': clientes})
def editar_cliente(request):
	chave = None
	conta = ''
	banco = ''
	local = ''
	cliente = ''
	descricao = ''
	try:
		request.GET['l']
		local = 'get'
	except:
		try:
			request.POST['l']
			local = 'post'
		except:
			redirect('exibir-cliente/')
	if local == 'get':	
		try:
			chave = request.GET['id'].split("]")
			conta = chave[0]
			banco = chave[1]
			descricao = request.GET['descricao']
		except:
			conta = ""
			banco = -2
	elif local == 'post':
		try:
			conta = request.POST['conta']
			banco = request.POST['banco']
			cliente = request.POST['cliente']
			descricao = request.POST['descricao']
		except:
			conta = ''
			banco = -1

	try:
		verificar = request.POST['verificar']
	except:
		verificar = False

	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	mycursor.execute("SELECT * FROM CONTA WHERE NR LIKE %s AND BANCO LIKE %s;", (conta, banco))
	dados = mycursor.fetchall()
	
	mensagem = None
	if verificar != False:
		try:
			mycursor.execute("UPDATE CONTA SET CLIENTE = %s, DESCRICAO = %s WHERE NR LIKE %s AND BANCO LIKE %s", (cliente, descricao, conta, banco))
			mydb.commit()
			mensagem = "Campo alterado com sucesso!"
		except:
			mensagem = "Ocorreu um erro na solicitação."
	elif dados != []:
		cliente = dados[0][1]
		descricao = dados[0][2]

	return render(request, 'editar_cliente.html', {"banco": banco, "conta": conta, "mensagem": mensagem, 'cliente': cliente, 'descricao': descricao})

def carga(request):
	args = 'py leitura_arquivos.py'
	p = subprocess.Popen(args)
	return redirect('/')
def apagar(request):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	mycursor.execute("DELETE FROM LANCAMENTOS;")
	mydb.commit()
	mycursor.execute("DELETE FROM CONTA;")
	mydb.commit()
	return redirect('/')
def listar_cliente(request):
	total = 0.00
	try:
		l = request.POST['id'].split("+")
		conta = l[0]
		banco = l[1]
	except KeyError:
		conta = None
		banco = None
		tag_selecionada = ''
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	clientes = []
	lista = []
	mycursor = mydb.cursor()

	mycursor.execute("SELECT * FROM CONTA ORDER BY CLIENTE;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['conta'] = dado[0]
		temp['nome'] = dado[1]
		temp['banco'] = str(dado[4])
		temp['descricao'] = dado[2]
		clientes.append(temp)
	if conta != None and banco != None:
		if conta == "0" and banco == "0":
			mycursor.execute("SELECT * FROM LANCAMENTOS;")
		else:
			mycursor.execute("SELECT * FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s", (conta, banco))
		dados = mycursor.fetchall()
		for dado in dados:
			temp = {}
			temp['banco'] = dado[0]
			temp['conta'] = dado[1]
			temp['id'] = dado[2]
			temp['descricao'] = dado[3]
			temp['operacao'] = dado[4]
			temp['valor_real'] = dado[5]
			temp['valor_dolar'] = dado[6]
			temp['tag'] = dado[9]
			temp['vencimento'] = str(dado[11].day) + '/' + str(dado[11].month) + '/' + str(dado[11].year)
			if 'Pagamentos' not in temp['tag']:
				total += temp['valor_real']
			lista.append(temp) 
	return render(request, 'listar_cliente.html', {'clientes': clientes, 'linhas': lista, 'id_conta': str(conta), 'id_banco': str(banco), 'total': round(total,2)})

def relatorio1(request):
	total = 0.00
	try:
		l = request.POST['id'].split("+")
		conta = l[0]
		banco = l[1]
	except KeyError:
		conta = None
		banco = None
		tag_selecionada = ''
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	clientes = []
	lista = []
	mycursor = mydb.cursor()

	mycursor.execute("SELECT * FROM CONTA ORDER BY CLIENTE;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['conta'] = dado[0]
		temp['nome'] = dado[1]
		temp['banco'] = str(dado[4])
		temp['descricao'] = dado[2]
		clientes.append(temp)
	tags = []
	if conta != None and banco != None:
		if conta != "0" and banco != "0":
			mycursor.execute("SELECT DISTINCT DESCRICAO, TAG FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s ORDER BY DESCRICAO;", (conta, banco))
		else:	
			mycursor.execute("SELECT DISTINCT DESCRICAO, TAG FROM LANCAMENTOS ORDER BY DESCRICAO;")
		result = mycursor.fetchall()
		for item in result:
			tag = {}
			tag['descricao'] = item[0]
			tag['tag'] = item[1]
			tags.append(tag)
	return render(request, 'relatorio1.html', {'tags' : tags, "clientes": clientes, 'id_conta': str(conta), 'id_banco': str(banco)})

def relatorio2(request):
	total = 0.00
	try:
		l = request.POST['id'].split("+")
		conta = l[0]
		banco = l[1]
		ano = request.POST['ano']
	except KeyError:
		conta = None
		banco = None
		ano = "2019"
		tag_selecionada = ''
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	clientes = []
	lista = []
	mycursor = mydb.cursor()

	mycursor.execute("SELECT * FROM CONTA ORDER BY CLIENTE;")
	dados = mycursor.fetchall()
	for dado in dados:
		temp = {}
		temp['conta'] = dado[0]
		temp['nome'] = dado[1]
		temp['banco'] = str(dado[4])
		temp['descricao'] = dado[2]
		clientes.append(temp)
	tags = {}
	if conta != None and banco != None:
		# print(conta)
		# print(banco)
		tags = []
		mycursor.execute("SELECT DISTINCT TAG FROM LANCAMENTOS ORDER BY TAG;")
		result = mycursor.fetchall()
		for tag in result:
			if 'Pagamentos' in tag[0]:
				continue
			tags.append({"tag": tag[0], "meses": {}})
		#print(tags)
		meses = {"01": "JAN",  "2": "FEV", "3": "MAR", "4": "ABR", "5": "MAI", "6": "JUN", "7": "JUL", "8": "AGO", "9": "SET", "10": "OUT", "11": "NOV", "12": "DEZ"}
		for tag in tags:
			for mes in meses:
				if conta == "0" and banco == "0":
					mycursor.execute("SELECT SUM(VALOR_REAL) FROM LANCAMENTOS WHERE TAG LIKE %s AND YEAR(DATA_VENCIMENTO) = %s AND MONTH(DATA_VENCIMENTO) = %s;", (tag['tag'], ano, mes))
				else:
					mycursor.execute("SELECT SUM(VALOR_REAL) FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s AND TAG LIKE %s AND YEAR(DATA_VENCIMENTO) = %s AND MONTH(DATA_VENCIMENTO) = %s;",  (conta, banco,tag['tag'], ano, mes))
				result = mycursor.fetchall()
				if result[0][0] == None:
					tag["meses"][meses[mes]] = "-"	
				else:
					tag["meses"][meses[mes]] = round(result[0][0], 2)
		#print(tags)
		# dados = mycursor.fetchall()
		# for dado in dados:
		# 	temp = {}
		# 	temp['banco'] = dado[0]
		# 	temp['conta'] = dado[1]
		# 	temp['id'] = dado[2]
		# 	temp['descricao'] = dado[3]
		# 	temp['operacao'] = dado[4]
		# 	temp['valor_real'] = dado[5]
		# 	temp['valor_dolar'] = dado[6]
		# 	temp['tag'] = dado[9]
		# 	temp['vencimento'] = str(dado[11].day) + '/' + str(dado[11].month) + '/' + str(dado[11].year)
		# 	if 'Pagamentos' not in temp['tag']:
		# 		total += temp['valor_real']
		# 	lista.append(temp) 
	return render(request, 'relatorio2.html', {'ano': ano, 'tags' : tags, "clientes": clientes, 'id_conta': str(conta), 'id_banco': str(banco)})



def retornaMaior(item):
	return float(item['valor'])

def gerar_relatorio(request):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()

	mycursor.execute("SELECT DISTINCT BANCO, CONTA FROM LANCAMENTOS;")
	dados = mycursor.fetchall()
	clientes = []
	clientes2 = []
	todos = {}
	todos['banco'] = "-1"
	todos['conta'] = "-1"
	mycursor.execute("SELECT DISTINCT DATA_VENCIMENTO FROM LANCAMENTOS ORDER BY DATA_VENCIMENTO DESC;")
	todos['vencimentos'] = []
	lista = mycursor.fetchall()
	for item in lista:
		string = str(item[0]).split("-")
		indice = None
		for i in range(0, len(todos['vencimentos'])):
			#print(todos['vencimentos'][i]['referencia'])
			if string[1] + '/' + string[0] == todos['vencimentos'][i]['referencia']:
				indice = i
				break
		if indice == None:
			dicionario = {}
			dicionario['referencia'] = string[1] + '/' + string[0]
			mycursor.execute("SELECT VALOR_REAL, DESCRICAO FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s AND VALOR_REAL = (SELECT MAX(VALOR_REAL) FROM LANCAMENTOS L WHERE L.DATA_VENCIMENTO = %s);", (item[0], item[0]))
			valores = mycursor.fetchall()
			
			dicionario['maior_lancamento'] = {"valor": str(valores[0][0]), "descricao": valores[0][1]}
			dicionario['tags'] = []
			mycursor.execute("SELECT SUM(VALOR_REAL), TAG FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s GROUP BY TAG;", (item[0], ))
			valores = mycursor.fetchall()
			
			mycursor.execute("SELECT DISTINCT TAG FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s;", (item[0], ))
			tags = mycursor.fetchall()

			for i in range(0, len(valores)):
				if 'Pagamentos' in tags[i][0]:
					continue
				tag = {}
				tag['nome'] = tags[i][0]
				tag['valor'] = float(valores[i][0])
				dicionario['tags'].append(tag)
			todos['vencimentos'].append(dicionario)
		else:
			mycursor.execute("SELECT VALOR_REAL, DESCRICAO FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s AND VALOR_REAL = (SELECT MAX(VALOR_REAL) FROM LANCAMENTOS L WHERE L.DATA_VENCIMENTO = %s);", (item[0], item[0]))
			valores = mycursor.fetchall()
			
			todos['vencimentos'][indice]['maior_lancamento'] = max(todos['vencimentos'][indice]['maior_lancamento'], {"valor": str(valores[0][0]), "descricao": valores[0][1]}, key=retornaMaior)
			# todos['vencimentos'][indice]['tags'] = []
			mycursor.execute("SELECT SUM(VALOR_REAL), TAG FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s GROUP BY TAG;", (item[0], ))
			valores = mycursor.fetchall()
			
			mycursor.execute("SELECT DISTINCT TAG FROM LANCAMENTOS WHERE DATA_VENCIMENTO = %s;", (item[0], ))
			tags = mycursor.fetchall()

			for i in range(0, len(valores)):
				if 'Pagamentos' in tags[i][0]:
					continue
				controle = None
				for j in range(len(todos['vencimentos'][indice]['tags'])):
					if todos['vencimentos'][indice]['tags'][j]['nome'] == tags[i][0]:
						controle = j
						break
				if controle != None:
					todos['vencimentos'][indice]['tags'][j]['valor'] += float(valores[i][0])
				else:
					tag = {}
					tag['nome'] = tags[i][0]
					tag['valor'] = float(valores[i][0])
					todos['vencimentos'][indice]['tags'].append(tag)
	clientes.append(todos)

	for dado in dados:
		temp = {}
		temp['banco'] = dado[0]
		temp['conta'] = dado[1]
		temp['vencimentos'] = []
		mycursor.execute("SELECT CLIENTE FROM CONTA WHERE NR LIKE %s AND BANCO LIKE %s;", (dado[1], dado[0]))
		temp['nome'] = mycursor.fetchall()[0][0]
		mycursor.execute("SELECT DISTINCT DATA_VENCIMENTO FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s ORDER BY DATA_VENCIMENTO DESC;",(dado[1], dado[0]))
		
		vencimentos = mycursor.fetchall()
		
		for vencimento in vencimentos:
			string = str(vencimento[0]).split("-")
			dicionario = {}
			dicionario['referencia'] = string[1] + '/' + string[0]
			mycursor.execute("SELECT VALOR_REAL, DESCRICAO FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s AND DATA_VENCIMENTO = %s AND VALOR_REAL = (SELECT MAX(VALOR_REAL) FROM LANCAMENTOS L WHERE L.CONTA LIKE %s AND L.BANCO LIKE %s AND L.DATA_VENCIMENTO = %s);", (dado[1], dado[0], vencimento[0], dado[1], dado[0], vencimento[0]))
			valores = mycursor.fetchall()
			
			dicionario['maior_lancamento'] = {"valor": str(valores[0][0]), "descricao": valores[0][1]}
			dicionario['tags'] = []
			mycursor.execute("SELECT SUM(VALOR_REAL), TAG FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s AND DATA_VENCIMENTO = %s GROUP BY TAG;", (dado[1], dado[0], vencimento[0]))
			valores = mycursor.fetchall()
			
			mycursor.execute("SELECT DISTINCT TAG FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s AND DATA_VENCIMENTO = %s;", (dado[1], dado[0], vencimento[0]))
			tags = mycursor.fetchall()

			for i in range(0, len(valores)):
				if 'Pagamentos' in tags[i][0]:
					continue
				tag = {}
				tag['nome'] = tags[i][0]
				tag['valor'] = str(valores[i][0])
				dicionario['tags'].append(tag)
			temp['vencimentos'].append(dicionario)
		clientes2.append(temp)
		clientes.append(temp)
	json_serializado = json.dumps(clientes)
	return render(request, 'gerar_relatorio.html', {'clientes': clientes2, 'dados': json_serializado})

def apagar_extrato(request):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	mensagem = None
	try:
		string = request.POST['data'].split("+")
		data_vencimento = string[0]
		banco = string[1]
		conta = string[2]
		try:
			mycursor.execute("DELETE FROM LANCAMENTOS WHERE CONTA LIKE %s AND BANCO LIKE %s AND DATA_VENCIMENTO = %s", (conta, banco, data_vencimento))
			mydb.commit()
			mensagem = "Exclusão feita com sucesso."
		except Exception as e:
			print(e)
			mensagem = "Ocorreu um erro."
	except:
		data_vencimento = ''
		banco = ''
		conta = ''

	mycursor.execute("SELECT DISTINCT BANCO, CONTA, DATA_VENCIMENTO FROM LANCAMENTOS ORDER BY BANCO, CONTA, DATA_VENCIMENTO;")
	dados = mycursor.fetchall()
	arquivos = []
	for dado in dados:
		temp = {}
		temp['banco'] = dado[0]
		temp['conta'] = dado[1]
		temp['data'] = dado[2]
		temp['data_formatada'] =  str(dado[2].year) + '-' + str(dado[2].month) + '-' + str(dado[2].day)
		arquivos.append(temp)

	return render(request, "apagar_extrato.html", {"dados": arquivos, "mensagem": mensagem })	