from django.apps import AppConfig


class BdrAppConfig(AppConfig):
    name = 'bdr_app'
