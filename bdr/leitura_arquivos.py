from threading import Thread
import csv
import mysql.connector
import datetime
import time
import os
arquivos_bancos = {}
BANCOS = {'BB': str(1), 'INTER': str(77)}
for banco in BANCOS:
	arquivos_bancos[banco] = []
	for _, _, arquivo in os.walk('CONTAS/'+ banco +'/EXTRATOS/'):
		arquivos_bancos[banco].append(arquivo)
# cartao = ''
# vencimento = ''
# DIRETORIO = ''
BASE = ''
ANO = ''
arquivo_tags = open("tags.txt", "r")
dicionario = arquivo_tags.read()
lista_tags = eval(dicionario)
def define_tag(lancamento):
	if lancamento['parcelada']:
		return 'Compras parceladas'
	for chave in lista_tags:
		if chave in lancamento['descricao'].replace(" ", "").upper():
			return lista_tags[chave]
	return 'Outros'
def extrato_existente(BANCO, cartao, vencimento):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	#mydb = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\Users\Carlos\Documents\BDR\bdr.accdb;')
	mycursor = mydb.cursor()
	sql = "SELECT count(*) FROM LANCAMENTOS WHERE BANCO = %s AND CONTA like %s AND DATA_VENCIMENTO = %s AND TEMPO LIKE 'A';"
	vals = (BANCO, cartao, vencimento)
	mycursor.execute(sql, vals)
	a = mycursor.fetchall()
	if a[0][0] != 0:
		return True
	#mydb.close()
def formata_data(nova_data): #retorna a data para o padrão do MySQL
	dia = nova_data.day
	mes = nova_data.month
	if int(nova_data.day) < 10:
		dia = '0' + str(nova_data.day)
	if int(nova_data.month) < 10:
		mes = '0' + str(nova_data.month)    
	return str(nova_data.year)+ '-' + str(mes) + '-' + str(dia)

def cadastra_cartao(cartao, banco, cliente, descricao, diretorio):
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	#mydb = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\Users\Carlos\Documents\BDR\bdr.accdb;')
	mycursor = mydb.cursor()
	sql = "SELECT * FROM CONTA WHERE NR=%s and BANCO=%s;"
	vals = (cartao, str(banco))
	mycursor.execute(sql, vals)
	if mycursor.fetchall() == []:
		sql = "INSERT INTO CONTA(NR, BANCO, CLIENTE, DESCRICAO, DIRETORIO) VALUES(%s, %s, %s, %s, %s);"
		vals = (cartao, banco, cliente, descricao, diretorio)
		mycursor.execute(sql, vals)
		mydb.commit()
	mydb.close()


def leitura_arquivo_inter(nome_arquivo): #lê o arquivo do banco inter
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	mycursor = mydb.cursor()
	json_final = {}
	lancamentos_temp = []
	lancamentos = []
	DIRETORIOT = 'CONTAS/INTER/'
	DIRETORIO = DIRETORIOT +'EXTRATOS/'
	with open( DIRETORIO + nome_arquivo, 'r') as f:
		linhas = csv.reader(f, delimiter=';')
		lancamentos_temp = list(linhas) #transforma o csv em uma lista

	indice = 0
	while 'Data da Transacao' not in lancamentos_temp[indice]:
		if 'Cartao ' in lancamentos_temp[indice]:
			cartao = lancamentos_temp[indice][1]
		elif 'Vencimento ' in lancamentos_temp[indice]:
			dados = lancamentos_temp[indice][1].split("/")
			vencimento = dados[2] + '-' + dados[1] + '-' + dados[0]
		indice += 1
	cartao = cartao.replace("X", "*").replace("x", "*")
	if extrato_existente(BANCOS['INTER'], cartao, vencimento):
		print(nome_arquivo + " já existente!")
		return
	indice += 1

	for i in range(indice, len(lancamentos_temp)): #a partir daqui, sei que só existem lançamentos
		info = {}
		info['mes'] = int(lancamentos_temp[i][0][3:5])
		info['dia'] = int(lancamentos_temp[i][0][0:2])
		info['descricao'] = lancamentos_temp[i][1]
		info['parcelada'] = True if 'Parcela' in lancamentos_temp[i][2] else False
		info['valor_real'] = float(lancamentos_temp[i][3].replace(",","."))
		info['valor_dolar'] = 0.00
		if info['parcelada']:
			info['parcela'] = int(lancamentos_temp[i][2].split(" ")[1].split("/")[0])
			info['total_parcelas'] = int(lancamentos_temp[i][2].split(" ")[1].split("/")[1])
		info['operacao'] = 'C'
		if info['parcelada']:
			info['tag'] = "Compras parceladas"
		else:
			tag = None
			mycursor.execute("SELECT DISTINCT DESCRICAO, TAG FROM LANCAMENTOS;")
			dados = mycursor.fetchall()
			for dado in dados:
				if lancamentos_temp[i][1].upper() in dado[0].upper() or dado[0].upper() in lancamentos_temp[i][1].upper() in dado[0].upper():
					tag = dado[1]
					break
			if tag == None:
				info['tag'] = define_tag(info)
			else:
				info['tag'] = tag
		lancamentos.append(info)
	json_final['cartao'] = cartao.replace("X", "*").replace("x", "*")
	json_final['vencimento'] = vencimento
	json_final['lancamentos'] = lancamentos
	cadastra_cartao(json_final['cartao'], str(77), '', 'Cartão Inter', DIRETORIOT)
	carga_banco(nome_arquivo, str(77), json_final, DIRETORIO)

def busca_dolar(linha): #procura o valor_dolar no lançamento do BB
	i = len(linha) - 1
	while linha[i] != ' ':
		i -= 1
	i += 1
	return float(linha[i:].replace(",", "."))

def leitura_arquivo_ourocard(nome_arquivo): #lê o arquivo do BB
	json_final = {}
	DIRETORIOT = 'CONTAS/BB/'
	DIRETORIO = DIRETORIOT + 'EXTRATOS/'
	arquivo = open(DIRETORIO + nome_arquivo, "r")
	linhas = arquivo.readlines()
	i = 0


	while 'Cliente         : ' not in linhas[i]:
		i += 1
	cliente = linhas[i].replace("Cliente         : ", "").replace("\n", "")
	i += 1
	cartao = linhas[i].replace("Nr.Cartão       : ", "").replace("\n", "").replace(" ", "")
	i += 1

	while 'Vencimento      : ' not in linhas[i]:
		i+= 1

	vencimento_temporario = linhas[i].replace("Vencimento      : ", "").replace(" ", "").replace("\n", "").split(".")
	vencimento = vencimento_temporario[2] + '-' + vencimento_temporario[1] + '-' + vencimento_temporario[0]
	if extrato_existente(BANCOS['BB'], cartao, vencimento):
		print(nome_arquivo + " já existente!")
		return

	while 'SALDO FATURA ANTERIOR' not in linhas[i]:
		i += 1


	lancamentos = []
	i += 1

	while True:
		
		while linhas[i] == '                                                     \n' or linhas[i] == ' \n':
			i += 1
		if 'SubTotal' in linhas[i]:
			break
		tag = linhas[i].replace(" ", "").replace("\n", "")
		if 'Outros' in tag:
			tag = 'Outros'
		
		if 'Compras parceladas' in linhas[i]:
			tag = linhas[i][9:27]
		i += 1
		
		while linhas[i] != '                                                     \n' and linhas[i] != ' \n':
			linha = linhas[i]
			compra = {}
			compra['descricao'] = linhas[i][10:33]
			compra['parcelada'] = False
			if 'Compras parceladas' in tag:
				linha = linhas[i].replace("PARC ", "")
				linha_sem_parcela = linhas[i].split("PARC ")[1]
				compra['parcela'] =  int(linha_sem_parcela[0:2])
				compra['total_parcelas'] = int(linha_sem_parcela[3:6])
				compra['descricao'] = linhas[i][10:24]
				compra['parcelada'] = True
			data = linhas[i][0:10].split(".")
			compra['mes'] = data[1]
			compra['dia'] = data[0]
			compra['tag'] = tag
			compra['valor_real'] = float(linhas[i][50:69].replace(" ", "").replace(".","").replace(",","."))
			compra['valor_dolar'] = busca_dolar(linhas[i])
			compra['operacao'] = 'C'
			lancamentos.append(compra)
			i += 1
	# for i in range(0, len(lancamentos)):
	# 	posicao = len(lancamentos[i]['descricao']) - 1
	# 	while lancamentos[i]['descricao'][posicao] == ' ':
	# 		posicao -= 1
	# 	lancamentos[i]['descricao'] = lancamentos[i]['descricao'][0:posicao + 1]
	json_final['cartao'] = cartao.replace(" ", "").replace("X", "*").replace("x", "*")
	json_final['cliente'] = cliente
	json_final['lancamentos'] = lancamentos
	json_final['vencimento'] = vencimento
	cadastra_cartao(json_final['cartao'], str(1), json_final['cliente'], 'Cartão OUROCARD', DIRETORIOT)
	carga_banco(nome_arquivo, str(1), json_final, DIRETORIO)

def carga_banco(nome_arquivo, BANCO, json_final, DIRETORIO): #função para dar carga ao banco. para cada arquivo, a função é chamada.
	mydb = mysql.connector.connect(
	host="localhost",
	user="root",
	database="bdr",
	password="1234",
	auth_plugin='mysql_native_password'
	)
	#mydb = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=C:\Users\Carlos\Documents\BDR\bdr.accdb;')
	mycursor = mydb.cursor()
	DIRETORIO_SQL = '//' + DIRETORIO.replace("/", "//")
	ARQUIVO_EXTRATO =  DIRETORIO_SQL + nome_arquivo
	mycursor.execute("DELETE FROM LANCAMENTOS WHERE TEMPO LIKE 'F' AND BANCO LIKE %s AND CONTA LIKE %s AND DATA_VENCIMENTO LIKE %s",(BANCO, json_final['cartao'], json_final['vencimento']))
	mydb.commit()
	for lancamento in json_final['lancamentos']:
		if lancamento['parcelada']:
			sql = "INSERT INTO LANCAMENTOs(BANCO, CONTA, DESCRICAO, TIPO, VALOR_REAL, MES, DIA, DATA_VENCIMENTO, TAG, ARQUIVO_EXTRATO, VALOR_DOLAR, TEMPO) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 'A')"
			val = (BANCO, json_final['cartao'], lancamento['descricao'] +' '+ str(lancamento['parcela']) + '/' + str(lancamento['total_parcelas']), lancamento['operacao'], lancamento['valor_real'], lancamento['mes'], lancamento['dia'], json_final['vencimento'], lancamento['tag'], ARQUIVO_EXTRATO, lancamento['valor_dolar'])
			mycursor.execute(sql, val)
			mydb.commit()


			controle = 1
			vencimento_separado = json_final['vencimento'].split("-")
			for i in range(lancamento['parcela'] + 1, lancamento['total_parcelas'] + 1):
				mes_atual = int(vencimento_separado[1]) + controle
				ano_atual = int(vencimento_separado[0])
				if mes_atual > 12:
					mes_atual -= 12
					ano_atual += 1
				nova_data = datetime.date(ano_atual, mes_atual, int(vencimento_separado[2]))
				sql = "SELECT * FROM LANCAMENTOS WHERE BANCO like %s AND CONTA like %s AND DESCRICAO like %s AND TIPO like %s AND VALOR_REAL >= %s AND VALOR_REAL <= %s AND MES=%s AND DIA=%s AND DATA_VENCIMENTO LIKE %s;"
				vals = (BANCO, json_final['cartao'], lancamento['descricao']+' ' + str(i) + '/' + str(lancamento['total_parcelas']) , lancamento['operacao'], lancamento['valor_real'] - 1, lancamento['valor_real'] + 1, lancamento['mes'], lancamento['dia'], nova_data)
				mycursor.execute(sql, vals)
				RESULT = mycursor.fetchall()
				if RESULT != []:
					break
				sql = "INSERT INTO LANCAMENTOS(BANCO, CONTA, DESCRICAO, TIPO, VALOR_REAL, MES, DIA, DATA_VENCIMENTO, TAG, ARQUIVO_EXTRATO, VALOR_DOLAR, TEMPO) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 'F')"
				val = (BANCO, json_final['cartao'], lancamento['descricao'] +' '+ str(i) + '/' + str(lancamento['total_parcelas']), lancamento['operacao'], lancamento['valor_real'], lancamento['mes'], lancamento['dia'], nova_data, lancamento['tag'], ARQUIVO_EXTRATO, lancamento['valor_dolar'])
				mycursor.execute(sql, val)
				mydb.commit()
				controle += 1
		else:
			sql = "INSERT INTO LANCAMENTOs(BANCO, CONTA, DESCRICAO, TIPO, VALOR_REAL, MES, DIA, DATA_VENCIMENTO, TAG, ARQUIVO_EXTRATO, VALOR_DOLAR, TEMPO) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 'A')"
			val = (BANCO, json_final['cartao'], lancamento['descricao'], lancamento['operacao'], lancamento['valor_real'], lancamento['mes'], lancamento['dia'], json_final['vencimento'], lancamento['tag'], ARQUIVO_EXTRATO, lancamento['valor_dolar'])
			mycursor.execute(sql, val)
			mydb.commit()
	mydb.close()
def inter():
	threads = []
	for arquivo in arquivos_bancos['INTER'][0]:
		#leitura_arquivo_inter(arquivo)
	#print(arquivo)
		thread = Thread(target=leitura_arquivo_inter, args=[arquivo])
		threads.append(thread)
	for thread in threads:
		thread.start()
		thread.join()
		
def ourocard():
	threads = []
	for arquivo in arquivos_bancos['BB'][0]:
		#leitura_arquivo_ourocard(arquivo)
	#print(arquivo)
		thread = Thread(target=leitura_arquivo_ourocard, args=[arquivo])
		threads.append(thread)
	for thread in threads:
		thread.start()
		thread.join()
antes = time.time()
# inter()
# ourocard()
thread1 = Thread(target=inter,args=[])
thread2 = Thread(target=ourocard,args=[])
thread1.start()
thread2.start()

thread1.join()
thread2.join()
depois = time.time()
print((depois-antes))